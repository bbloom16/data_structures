package Question1;

import java.util.*;

public class BSTExtended <E extends Comparable> 
							extends BinarySearchTree<E> {

	public BSTExtended(){
		super();
	}//end default constructor
	
	public void levelOrder(){
		Queue<Node> q = new Queue<Node>();//new queue
		q.insert(root);//insert root into queue
		
		while(!q.empty()){
			Node x = q.remove();//remove the front element
			System.out.print(x.data + " ");
			
			if(x.left!=null){
				q.insert(x.left);
			}
			if(x.right!=null){
				q.insert(x.right);
			}
			
		}//while q is not empty
		
		
	}//end method levelOrder
	
	public void displayTree(){
		recurdisplayTree(root);
	}//end method displayTree
	
	public void recurdisplayTree(Node n){
		
		if(isLeaf(n)==false){
			System.out.print(n.data + "(");
			
			if(n.left!=null){
				recurdisplayTree(n.left);
			}
			else{
				System.out.print("-");
			}

			if(n.right!=null){
				recurdisplayTree(n.right);
			}
			else{
				System.out.print("-");
			}
			
			System.out.print(")");
		}//node is not a leaf
		else{
			System.out.print(n.data);
			
		}//node is a leaf
	}//end method recurdisplayTree
	
	  public boolean isLeaf(Node n){
     	 if((n.left==null) && (n.right==null))
     		 return true;
     	 else{
     		 return false;
     	 }
      }//end method isLeaf
	  
	  public static void main(String [] args){
		 Scanner kb = new Scanner(System.in);
		  
		 BSTExtended<Integer> bt = new BSTExtended<Integer>();
		  
		 System.out.print("Enter an integer to add to queue, or 0 to end: ");
		 int input = kb.nextInt();
		 
		 while(input!=0){
			 bt.insert(input);
			 
			 System.out.print("Enter an integer to add to queue, or 0 to end: ");
			 input = kb.nextInt();
		 }//all integers inserted
		 
		 
		 System.out.println();
		 System.out.println("Level Order: ");
		 bt.levelOrder();
		 System.out.println();
		 System.out.println("Display Tree: ");
		 bt.displayTree();
		 
	  }//end main method
	
}//end class BSTExtended
