package Question2;

import java.io.*;
import java.util.*;

public class GuessNode implements Serializable{
	public String data;
	public GuessNode yes;
	public GuessNode no;
	
	public GuessNode(){
		data = null;
		yes = null;
		no = null;
	}//end default constructor
	
	public GuessNode(String data){
		this.data = data;
		yes = null;
		no = null;
	}//end constructor
	
}//end class GuessNode