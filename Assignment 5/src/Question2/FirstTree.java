package Question2;

import java.io.*;
import java.util.*;

public class FirstTree implements Serializable{
	GuessNode root;
	
	public FirstTree() {
		root = new GuessNode("Are they a person?");
		root.yes = new GuessNode("Harriet Tubman");
		root.no = new GuessNode("telephone");
	}//end default constructor
		
	public static void main(String[] args) throws IOException {
		//IMPORTANT: Only run this method to reset the entire tree
		
		FirstTree root = new FirstTree();
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("GuessingTree.dat"));
		output.writeObject(root);
	}//end main method
	
}//end class FirstTree