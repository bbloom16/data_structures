package Question2;

import java.io.*;
import java.util.*;

public class GuessingGame implements Serializable{
	
	FirstTree t;
	
	public GuessingGame() throws IOException, ClassNotFoundException {
	// Read the tree object from disk
	ObjectInputStream input = new ObjectInputStream(new FileInputStream("GuessingTree.dat"));
	
	t = (FirstTree)input.readObject();
	}//end default constructor
	
	public boolean isLeaf(GuessNode p) {
		return (p.yes==null) && (p.no==null);
	}//end method isLeaf
	
	public void play() {
		Scanner kb = new Scanner(System.in);
		System.out.println("Think of an Object or person.");
		System.out.println("Enter 'y' or 'n' to answer questions.");
		
		GuessNode temp = t.root;
		
		String response = "";
		while(!isLeaf(temp)){
			System.out.println(temp.data);
			
			response = kb.next();
			response.toLowerCase();
			if(response.equals("y")){
				temp = temp.yes;
				
			}//yes direction
			else if(response.equals("n")){
				temp = temp.no;
			}//no direction
			
		}//while the node is not a leaf
		String tempLast = temp.data;
		System.out.println(tempLast);
		
		response = kb.next();
		response.toLowerCase();
		
		if(response.equals("y")){
			System.out.println("Got it!");
		}//computer guessed the correct response
		
		
		else if(response.equals("n")){
			kb.nextLine();
			System.out.println("Who or what were you thinking of?");
			String newResponse = kb.nextLine();
			System.out.println("Enter a question that distinguishes " + tempLast +" from " + newResponse);
			String newQuestion = kb.nextLine();
			
			temp.data = newQuestion;
			temp.no = new GuessNode(tempLast);
			temp.yes = new GuessNode(newResponse);
			
		}//computer did not guess the response
		
		
	}//play the game
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
	Scanner in = new Scanner (System.in); 
	GuessingGame g = new GuessingGame(); 
	String answer;
	do {
		g.play();
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("GuessingTree.dat"));
		output.writeObject(g.t.root);
		System.out.print("Play again? Y or y for yes any other key for no: "); answer = in.nextLine();
	}while (answer.equals("Y") || answer.equals("y"));
	System.out.println("Bye Bye.");
	// save the updated tree to the disk
	ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("GuessingTree.dat"));
	output.writeObject(g.t);
	
	}//end main method
	
}//end class GuessingGame
