package Question1;

public class Nodes implements Comparable{
	public String data;
	public Nodes p1;
	public Nodes p2;
	
	public Nodes(){
		data = "";
		p1 = null;
		p2 = null;
	}
	
	public Nodes(String data, Nodes p1, Nodes p2){
		this.data = data;
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public int compareTo(Object o){
		return data.compareTo(( (Nodes) o).data);
	}//end method compareTo
	
	public String toString(){
		return "Data: " + data; //". Pointer1: " + p1 + ". Pointer2: " + p2;
	}//end method toString
	
}//end class Node
