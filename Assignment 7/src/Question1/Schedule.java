package Question1;

import java.util.*;
import java.io.*;

public class Schedule {
	Scanner kb = new Scanner(System.in);//keyboard
	MultiList m = new MultiList();
	
	public Schedule(){
		File inputFile = new File("students.txt");//input file
		
		try{
			Scanner inputReader = new Scanner(inputFile);
			
			while(inputReader.hasNext()){
				String name = inputReader.next().toLowerCase();
				String course = inputReader.next().toLowerCase();
				
				m.insert(name, course);//store name and activity in nodes
			}//read in data from a file
			
		}//end try
		catch(FileNotFoundException e){
			System.out.println("Input file not found");
		}//end catch
		
		int choice = 0;
		String name;
		String course;
		
		while(choice!=4){
			displayMenu();
			choice = kb.nextInt();
			
			switch(choice){
			case 1:
				System.out.print("Course name: ");
				course = kb.next().toLowerCase();
				m.printCourse(course);
				//find all students in a course
				break;
			case 2:
				System.out.print("Student name: ");
				name = kb.next().toLowerCase();
				m.printStudent(name);
				//find all courses for a student
				break;
			case 3:
				System.out.print("Course name: ");
				course = kb.next().toLowerCase();
				System.out.print("Student name: ");
				name = kb.next().toLowerCase();
				m.insert(name, course);
				m.printFile();
				//add a student to a course
				break;
			case 4:
				//write to output file, then exit
				System.exit(0);//exit
				break;
			default:
				break;
			}//end switch
		}//end while
		
		
	}//end default constructor
	
	public void displayMenu(){
		System.out.println("----Menu----");
		System.out.println("1. Find all students in a course");
		System.out.println("2. Find all courses for a student");
		System.out.println("3. Add a student to a course");
		System.out.println("4. Exit");
		System.out.print("Choice: ");
	}//end method displayMenu
	
	public static void main(String [] args){
		Schedule s = new Schedule();
	}//end main method
	
}//end class Schedule
