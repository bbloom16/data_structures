package Question1;

import java.util.*;
import java.io.*;

public class MultiList {
	BinarySearchTree<Nodes> nameTree;
	BinarySearchTree<Nodes> courseTree;
	
	public MultiList(){
		nameTree = new BinarySearchTree<Nodes>();
		courseTree = new BinarySearchTree<Nodes>();
	}//end default constructor
	
	public void insert(String name, String course){
		//name node
		Nodes studentName = new Nodes();
		studentName.data = name;
		Nodes courseName = new Nodes();
		courseName.data = course;
		
		Nodes nameResult = (Nodes) nameTree.search(studentName);//searches for name in nameTree
		Nodes courseResult = (Nodes) courseTree.search(courseName);//searches for course in courseTree
		
		Nodes en;//enrollment node
		Nodes n;//name node
		Nodes c;//course node
		Nodes temp;//temp node
		Nodes en1;// additional enrollment node
		
		if(nameResult==null){

			if(courseResult==null){//name not, course not in system
				//System.out.println("course not, name not in system");
				
				en = new Nodes();//enrollment node
				n = new Nodes(name, en, null);//name node
				c = new Nodes(course, null, en);//course node
				
				en.p1 = n;
				en.p2 = c;
				
				nameTree.insert(n);
				courseTree.insert(c);
			}
			else{//name not, course yes in system
				//System.out.println("name not, course yes in system");
				
				en = courseResult.p2;
			
				Nodes p = new Nodes();
				Nodes q = new Nodes();
				p = en;
				q = null;
				while(!p.data.equals(course)){
					q=p;
					p = p.p2;
				}
				
				en1 = new Nodes();
				q.p2 = en1;
				en1.p2 = p;
				n = new Nodes(name, en1, null);//name node
				en1.p1 = n;
				n.p1 = en1;
				nameTree.insert(n);
			}
		}
		else{
			
			if(courseResult==null){//name yes, course not in system
				//System.out.println("name yes, course not in system");
				
				en = nameResult.p1;

				Nodes p = new Nodes();
				Nodes q = new Nodes();
				p = en;
				q = null;
				while(!p.data.equals(name)){
					q=p;
					p = p.p1;
				}
				
				en1 = new Nodes();
				c = new Nodes(course, null, en1);
				q.p1 = en1;
				en1.p1 = p;
				en1.p2 = c;
				c.p2 = en1;
			
				courseTree.insert(c);
			}
			
			else{//name yes, course yes in system
				//System.out.println("name yes, course yes in system");
			
				en1 = new Nodes();
				en = nameResult.p1;
				temp = courseResult.p2;
				
				Nodes p = new Nodes();
				Nodes q = new Nodes();
				p = en;
				q = null;
				while(!p.data.equals(name)){
					q=p;
					p = p.p1;
				}
				q.p1 = en1;
				en1.p1 = p;
				
				Nodes a = new Nodes();
				Nodes b = new Nodes();
				a = temp;
				b = null;
				while(!a.data.equals(course)){
					b=a;
					a = a.p2;
				}
				b.p2 = en1;
				en1.p2 = a;
				
			}
		}
		
	}//end method insert
	
	public void printCourse(String course){
		Nodes courseName = new Nodes();
		courseName.data = course;
		Nodes courseResult = courseTree.search(courseName);//look for course
		if(courseResult==null){
			System.out.println("Course does not currently exist.");
			return;
		}
		else{
			Nodes en = courseResult.p2;
			
			Nodes p = new Nodes();
			Nodes q = new Nodes();
			p = en;
			q = null;
			while(!p.data.equals(course)){
				q=p;
				System.out.println(p.p1.data);
				p = p.p2;
			}
		}
	}//end method printCourse
	
	public String printCourseGUI(String course){
		String printer = "";
		Nodes courseName = new Nodes();
		courseName.data = course;
		Nodes courseResult = courseTree.search(courseName);//look for course
		if(courseResult==null){
			return ("Course does not currently exist.");
		}
		else{
			Nodes en = courseResult.p2;
			
			Nodes p = new Nodes();
			Nodes q = new Nodes();
			p = en;
			q = null;
			while(!p.data.equals(course)){
				q=p;
				printer += (p.p1.data) + "\n";
				p = p.p2;
			}
			return printer;
		}
	}//end method printCourse
	
	public void printStudent(String name){
		Nodes studentName = new Nodes();
		studentName.data = name;
		Nodes nameResult = nameTree.search(studentName);//look for student
		if(nameResult==null){
			System.out.println("Student is not currently enrolled.");
			return;
		}
		else{
			Nodes en = nameResult.p1;
			
			Nodes p = new Nodes();
			Nodes q = new Nodes();
			p = en;
			q = null;
			while(!p.data.equals(name)){
				q=p;
				System.out.println(p.p2.data);
				p = p.p1;
			}
		}
	}//end method printStudent
	
	public String printStudentGUI(String name){
		String printer = "";
		Nodes studentName = new Nodes();
		studentName.data = name;
		Nodes nameResult = nameTree.search(studentName);//look for student
		if(nameResult==null){
			return ("Student is not currently enrolled.");
		}
		else{
			Nodes en = nameResult.p1;
			
			Nodes p = new Nodes();
			Nodes q = new Nodes();
			p = en;
			q = null;
			while(!p.data.equals(name)){
				q=p;
				printer += (p.p2.data) + "\n";
				p = p.p1;
			}
			return printer;
		}
	}//end method printStudent
	
	public void printFile(){
		File outputFile = new File("dataOutput.dat");
		try{
			FileWriter fw = new FileWriter(outputFile);
			
			//add writing names and courses to output file
		}//end try
		catch(Exception e){
			System.out.println("Output file not found.");
		}//end catch
	}//end method printFile
	
}//end class MultiList
