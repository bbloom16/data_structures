package Question1;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.*;


public class ScheduleGUI extends JFrame{
	//pannels
	private JPanel inputPanel;
	private JPanel outputPanel;
	private JPanel buttonPanel;
	private JPanel displayPanel;
	private JPanel submitPanel;
	
	//labels
	private JLabel inputLabel;
	private JLabel outputLabel;
	
	//text areas
	private JTextArea outputText;
	private JTextArea inputText;
	
	//buttons
	private JButton findStudents;
	private JButton findCourses;
	private JButton addStudent;
	private JButton exitButton;
	private JButton submitButton;
	
	//list
	MultiList m = new MultiList();
	
	//booleans
	private boolean students;
	private boolean courses;
	private boolean add;
	
	public ScheduleGUI(){
		super("ScheduleGUI");
		
		students = false;
		courses = false;
		add = false;
		
		buttonPanel = new JPanel();
		findStudents = new JButton("Find all students in a course");
		findCourses = new JButton("Find all courses for a student");
		addStudent = new JButton("Add a student to a course");
		exitButton = new JButton("Exit");
		findStudents.addActionListener(new ButtonListener());
		findCourses.addActionListener(new ButtonListener());
		addStudent.addActionListener(new ButtonListener());
		exitButton.addActionListener(new ButtonListener());
		buttonPanel.add(findStudents);
		buttonPanel.add(findCourses);
		buttonPanel.add(addStudent);
		buttonPanel.add(exitButton);
		add(buttonPanel, BorderLayout.SOUTH);
		
		
		displayPanel = new JPanel(new BorderLayout());
		inputLabel = new JLabel("Input:");
		displayPanel.add(inputLabel, BorderLayout.WEST);
		outputLabel = new JLabel("Output:");
		displayPanel.add(outputLabel, BorderLayout.EAST);
		add(displayPanel, BorderLayout.NORTH);
		
		inputPanel = new JPanel();
		
		inputText = new JTextArea(30, 35);
		JScrollPane inputTextScroll = new JScrollPane(inputText,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		inputPanel.add(inputText);
		add(inputPanel, BorderLayout.WEST);
		
		submitPanel = new JPanel(new BorderLayout());
		submitButton = new JButton("Submit");
		submitButton.addActionListener(new ButtonListener());
		submitPanel.add(submitButton, BorderLayout.CENTER);
		add(submitPanel, BorderLayout.CENTER);
		
		outputPanel = new JPanel();
		
		outputText = new JTextArea(30, 35);
		JScrollPane outputTextScroll = new JScrollPane(outputText,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		outputPanel.add(outputText);
		add(outputPanel, BorderLayout.EAST);
		
		
		
		
		File inputFile = new File("students.txt");//input file
		
		try{
			Scanner inputReader = new Scanner(inputFile);
			
			while(inputReader.hasNext()){
				String name = inputReader.next().toLowerCase();
				String course = inputReader.next().toLowerCase();
				
				m.insert(name, course);//store name and activity in nodes
			}//read in data from a file
			
		}//end try
		catch(FileNotFoundException e){
			outputText.setText("Input file not found");
		}//end catch
		
		
		
		setVisible(true);
		setResizable(true);
        setBounds(0,0,950,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}//end constructor
	
	
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){	
			if(e.getSource()==exitButton){
				System.exit(0);
			}//exit button
			else if(e.getSource()==findStudents){
				students = true;
				inputLabel.setText("Enter course name:");
				inputText.setText("");
				outputText.setText("");
				repaint();
			}//find students button
			else if(e.getSource()==findCourses){
				courses = true;
				inputLabel.setText("Enter student name:");
				inputText.setText("");
				outputText.setText("");
				repaint();
			}
			else if(e.getSource()==addStudent){
				add = true;
				inputLabel.setText("Enter new student name and course enrolled in:");
				inputText.setText("");
				outputText.setText("");
				repaint();
			}
			else if(e.getSource()==submitButton){
				if(students){
					
					String course = inputText.getText();
					String screen = m.printCourseGUI(course);
					outputText.setText(screen);
					repaint();
				}
				else if(courses){
					String name = inputText.getText();
					String screen = m.printStudentGUI(name);
					outputText.setText(screen);
					repaint();
				}
				else if(add){
					String student = inputText.getText();
					if(!student.equals("")){
						String[] splited = student.split("\\s+");
						m.insert(splited[0], splited[1]);
					}
					
					outputText.setText("");
				}
				//reset all to false
				students = false;
				courses = false;
				add = false;
				inputText.setText("");
			}
		}//end method actionPerformed
	}//end class ActionListener
	
	public static void main(String [] args){
		ScheduleGUI s = new ScheduleGUI();
	}//end main method
	
}//end class ScheduleGUI
