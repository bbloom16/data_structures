package Question1;

import java.io.*;
import java.util.*;

public class Maps {
	
	PriorityQueue<Node> pq;
	Node[] dataArr;
	String routeFinal = "";
	
	protected class Node implements Comparable{
		public String vertex;
		public int dist;//distance to start
		public String predessor;
		public boolean visited;
		public Node next;
		
		public Node(){
			vertex = null;
			dist = 0;
			predessor = null;
			visited = false;
			next = null;
		}//end default constructor
		
		public Node(String vertex, int dist, String predessor, boolean visited, Node next){
			this.vertex = vertex;
			this.dist = dist;
			this.predessor = predessor;
			this.visited = visited;
			this.next = next;
		}//end arg constructor
		
		public Node(String vertex, String predessor, int dist){
			this.vertex = vertex;
			this.predessor = predessor;
			this.dist = dist;
		}//end arg constructor
		
		public Node(Node n){
			this.vertex = n.vertex;
			this.dist = n.dist;
			this.predessor = n.predessor;
			this.visited = n.visited;
			this.next = n.next;
		}//end arg constructor
		
		public int compareTo(Object o){
			Integer a = new Integer(dist);
			Integer b = new Integer(((Node)o).dist);
			return a.compareTo(b);
		}//end method compareTo
		
		public String toString(){
			return vertex;
			//return "Vertex: " + vertex + ". Predecessor: " + predessor + ". Dist: " + dist;
		}//end method toString
		
		public void printData(){
			System.out.println("Vertex: " + vertex + ". Dist: " + dist + " | ");
		}//end method printData
		
		public void printPath(){
			System.out.println("Vertex: " + vertex + ". Predecessor: " + predessor + ". Dist: " + dist);
		}//end method printPath
		
	}//end class Data
	
	public Maps(){
		pq = new PriorityQueue<Node>(10000000);
		dataArr = new Node[325];//number of cities
		
		//read in city names
		
		try{
			
			InputStream cityFile = this.getClass().getClassLoader().getResourceAsStream("cityNames.txt");
			//FileReader cityFile = new FileReader("cityNames.txt");
			Scanner cityInput = new Scanner(cityFile);
			
			int i=0;
			while(cityInput.hasNextLine()){
				dataArr[i] = new Node();
				dataArr[i].vertex = cityInput.next();//set the name of each city to the input String
				dataArr[i].visited = false;//set each city to not visited
				i++;
			}//end while
			
		}//end try
		catch(Exception e){
			System.out.println("Error, city names file not found");
		}//end catch
		
		//read in map
		
		try{
			InputStream mapFile = this.getClass().getClassLoader().getResourceAsStream("map.txt");
			//FileReader mapFile = new FileReader("map.txt");
			Scanner mapInput = new Scanner(mapFile);
			
			while(mapInput.hasNextLine()){
				//System.out.println("new line");
				String firstCityName = mapInput.next();
				String secondCityName = mapInput.next();
				int distance = mapInput.nextInt();
				
				Node firstCity = findNode(firstCityName);
				Node secondCity = findNode(secondCityName);
				
				//first city
				Node p = firstCity;
				while(p.next!=null){
					p = p.next;
				}
				Node addedOne = new Node();
				addedOne.vertex = secondCityName;
				addedOne.dist = distance;
				p.next = addedOne;
				
				//second city
				Node q = secondCity;
				while(q.next!=null){
					q = q.next;
				}
				Node addedTwo = new Node();
				addedTwo.vertex = firstCityName;
				addedTwo.dist = distance;
				q.next = addedTwo;
				
			}//end while
			
		}//end try
		catch(Exception e){
			System.out.println("Error, map file not found");
		}//end catch
		
		
	}//end default constructor
	
	public int shortestPath(String start, String end){		
		int finalDist = 0;
		
		Node st = new Node(start, "-", 0);//make start node
		Node st1 = findNode(start);
		st1.visited = true;
		st1.predessor = "-";
		pq.insert(st);//insert start node into pq ([start] - 0)
		boolean done = false;
		
		while((!done) && (!pq.empty())){//while not done and not empty
			
			Node x = pq.remove();//remove the first entry from the queue
			Node v = findNode(x.vertex);
			
			if(!v.visited){
			
				v.visited = true;//mark visited
				v.dist = x.dist;//get distance stored in x and put on the graph structure
				v.predessor = x.predessor;//make predecessor of v equal to pred. of x
			}
			if((v.vertex).equals(end)){
				done = true;
			}//if v is the goal set done to true
			else{
				Node w = v.next;
				while(w.next!=null){
					if(!w.visited){
						int d = w.dist;
						w.visited = true;
						Node n = new Node(w.vertex, v.vertex, d);
						pq.insert(n);
					}
					w = w.next;
				}
			}
		}//end while
		if(!done){
			return -99;
		}
		else{
			Node path = new Node(findNode(end));
			while(!path.predessor.equals("-")){
				finalDist +=path.dist;
				routeFinal = routeFinal + "\n" + (path.vertex + " -> " + path.predessor + " | Dist: " + path.dist) ;
				path = findNode(path.predessor);
			}
		}
		
		return finalDist;
	}//end method shortestPath
	
	public String getRouteFinal(){
		return routeFinal;
	}//end method getRouteFinal
	
	public Node findNode(String vertexName){
		boolean found = false;
		Node result = new Node();
		for(int i=0; i<dataArr.length; i++){
			String temp = dataArr[i].vertex;
			if(temp.equals(vertexName)){
				//System.out.println("found node");
				found = true;
				result = dataArr[i];
				return result;//found city Node
			}
		}//end for, go through each city in array
		
		if(found){
			return result;//found node
		}
		else{
			//System.out.println("did not find node");
			result.vertex = "--";
			return null;//did not find node
		}
	}//end method findNode

	public void printNodes(){
		for(int i=0; i<dataArr.length; i++){
			System.out.println("======================= Node =================================");
			if(dataArr[i].predessor==null)System.out.println("Node: " + dataArr[i].vertex + " Visited: " + dataArr[i].visited + " Dist: " + dataArr[i].dist);
			else System.out.println("Node: " + dataArr[i].vertex + " Visited: " + dataArr[i].visited + " Pred: " + dataArr[i].predessor + " Dist: " + dataArr[i].dist);
			Node temp = new Node(dataArr[i]);
			while(temp.next!=null){
				temp = temp.next;
				temp.printData();
			}
		}
	}//end method printNodes
	
	
	/*public static void main(String [] args){
		Maps m = new Maps();		
	}//end main method*/
	
}//end class Maps
