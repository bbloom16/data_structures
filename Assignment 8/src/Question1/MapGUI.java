package Question1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MapGUI extends JFrame{
	//panels
	private JPanel boardPanel;
	private JPanel buttonPanel;
	private JPanel routePanel;
	
	//lists
	private JList origin;
	private JList dest;
	
	//buttons
	private JButton clearButton;
	private JButton computeButton;
	private JButton exitButton;
	
	//text areas
	private JTextField mileageTextArea;
	private JTextArea routeTextArea;
	
	//labels
	private JLabel originLabel;
	private JLabel destLabel;
	private JLabel milLabel;
	
	//scroll panes
	private JScrollPane originScroll;
	private JScrollPane destScroll;
	private JScrollPane routeScroll;
	
	//map
	Maps m;
	
	
	public MapGUI(){
		super("OOEY GOOEY MAPPY GUI");
		
		m = new Maps();
		
		boardPanel = new JPanel(new GridLayout());
		originLabel = new JLabel("Origin: ");
		origin = new JList(m.dataArr);
		origin.setVisibleRowCount(10);
		destLabel = new JLabel("Destination: ");
		dest = new JList(m.dataArr);
		dest.setVisibleRowCount(10);
		
		origin.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		dest.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		originScroll = new JScrollPane(origin);
		destScroll = new JScrollPane(dest);
		
		
		boardPanel.add(originLabel);
		boardPanel.add(originScroll);
		boardPanel.add(destLabel);
		boardPanel.add(destScroll);
		
		add(boardPanel, BorderLayout.NORTH);
		
		buttonPanel = new JPanel();
		milLabel = new JLabel("Mileage: ");
		mileageTextArea = new JTextField(50);
		clearButton = new JButton("Clear");
		computeButton = new JButton("Compute");
		exitButton = new JButton("Exit");
		
		clearButton.addActionListener(new ButtonListener());
		computeButton.addActionListener(new ButtonListener());
		exitButton.addActionListener(new ButtonListener());
		
		
		
		buttonPanel.add(milLabel);
		buttonPanel.add(mileageTextArea);
		buttonPanel.add(clearButton);
		buttonPanel.add(computeButton);
		buttonPanel.add(exitButton);
		
		add(buttonPanel, BorderLayout.CENTER);
		
		routePanel = new JPanel();
		routeTextArea = new JTextArea(15,25);
		routeScroll = new JScrollPane(routeTextArea);
		routeScroll.setMaximumSize(new Dimension(15, 25));
		
		routePanel.add(routeScroll);
		
		add(routePanel, BorderLayout.SOUTH);
		
		setVisible(true);
		setResizable(false);
        setBounds(0,0,600,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}//end constructor
	
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){	
			if(e.getSource()==computeButton){
				int startIndex = origin.getSelectedIndex();
				int endIndex = dest.getSelectedIndex();
				
				//System.out.println(m.dataArr[startIndex].vertex + " " + m.dataArr[endIndex].vertex);
				int result = m.shortestPath(m.dataArr[startIndex].vertex, m.dataArr[endIndex].vertex);
				if(result!=-99){
					mileageTextArea.setText(result+"");
					routeTextArea.setText(m.getRouteFinal());
				}//found route
				else{
					mileageTextArea.setText("-");
					routeTextArea.setText("Error, route could not be found");
				}//no route
				
				repaint();
			}//compute button
			else if(e.getSource()==clearButton){
				mileageTextArea.setText("");
				routeTextArea.setText("");
				
				repaint();
			}//clear button
			else{
				System.exit(0);
			}//exit button
		}
	}//end class ButtonListener
	
	public static void main(String [] args){
		MapGUI map = new MapGUI();
	}//end main
	
}//end class
