package Question1;

public class Board {
	char[][] board;
	int turn;
	static int rows = 6;
	static int cols = 7;
	
	static int[][] evaluationTable = 
		{{3, 4, 5, 7, 5, 4, 3}, 
		{4, 6, 8, 10, 8, 6, 4},
		{5, 8, 11, 13, 11, 8, 5}, 
		{5, 8, 11, 13, 11, 8, 5},
		{4, 6, 8, 10, 8, 6, 4},
		{3, 4, 5, 7, 5, 4, 3}};
	
	public Board(){
		board = new char[rows][cols];
		
		//initialize empty board
		for(int i=0; i<6; i++){
			for(int j=0; j<7; j++){
				board[i][j] = 'e';
			}//end inner for loop
		}//end outer for loop
		
		//initialize turn
		turn = 0;//turn set to user turn
		
	}//end default constructor
	
	public Board(Board b){
		board = new char[rows][cols];
		
		//initialize empty board
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				board[i][j] = b.board[i][j];
			}//end inner for loop
		}//end outer for loop
		
		//initialize turn
		turn = b.turn;
		
	}//end constructor
	
	public char winner(){
		int blackWins = 0;
		int redWins = 0;
		
		//VERTICAL
		for(int a=0; a<3; a++){//rows
			for(int b=0; b<cols; b++){//cols
				//check black
				if((board[a][b] == 'b') &&
				(board[a][b] == board[a+1][b]) &&
				(board[a][b] == board[a+2][b]) &&
				(board[a][b] == board[a+3][b])){
					blackWins++;
				}
				
				//check red
				if((board[a][b] == 'r') &&
				(board[a][b] == board[a+1][b]) &&
				(board[a][b] == board[a+2][b]) &&
				(board[a][b] == board[a+3][b])){
					redWins++;
				}
			}
		}//end vertical
		
		
		//HORIZONTAL
		for(int c=0; c<6; c++){//rows
			for(int d=0; d<4; d++){//cols
				//check black
				if((board[c][d] == 'b') &&
				(board[c][d] == board[c][d+1]) &&
				(board[c][d] == board[c][d+2]) &&
				(board[c][d] == board[c][d+3])){
					blackWins++;
				}
				
				//check red
				if((board[c][d] == 'r') &&
				(board[c][d] == board[c][d+1]) &&
				(board[c][d] == board[c][d+2]) &&
				(board[c][d] == board[c][d+3])){
					redWins++;
				}
			}
		}//end horizontal
		
		
		//RIGHT AND DOWN
		for(int e=0; e<3; e++){//rows
			for(int f=0; f<4; f++){//cols
				//check black
				if((board[e][f] == 'b') &&
				(board[e][f] == board[e+1][f+1]) &&
				(board[e][f] == board[e+2][f+2]) &&
				(board[e][f] == board[e+3][f+3])){
					blackWins++;
				}
				
				//check red
				if((board[e][f] == 'r') &&
				(board[e][f] == board[e+1][f+1]) &&
				(board[e][f] == board[e+2][f+2]) &&
				(board[e][f] == board[e+3][f+3])){
					redWins++;
				}
			}
		}
		
		
		//RIGHT AND UP
		for(int g=5; g>2; g--){//rows
			for(int h=0; h<4; h++){//cols
				//check black
				if((board[g][h] == 'b') &&
				(board[g][h] == board[g-1][h+1]) &&
				(board[g][h] == board[g-2][h+2]) &&
				(board[g][h] == board[g-3][h+3])){
					blackWins++;
				}
				
				//check red
				if((board[g][h] == 'r') &&
				(board[g][h] == board[g-1][h+1]) &&
				(board[g][h] == board[g-2][h+2]) &&
				(board[g][h] == board[g-3][h+3])){
					redWins++;
				}
			}
		}
		
		
		//looks at board. returns winner
		
		if(blackWins>0 && redWins>0){
			return 't';
		}//tie
		else if(blackWins>0 && redWins==0){
			return 'b';
		}//black wins
		else if(blackWins==0 && redWins>0){
			return 'r';
		}//red wins
		else{
			return 'x';
		}//no wins
		
		//return b if black wins, r if red wins, t for tie, or x if no winner
	}//end method winner
	
	public boolean done(){
		return winner()!='x';//must be r, b, or t
	}//end method done
	
	public int evaluationFunction(){
		int sum = 0;
		//System.out.println(winner());
		if(winner()=='r'){
			//System.out.println("eval black");
			return 10000;
		}
		else if (winner()=='b'){
			//System.out.println("eval red");
			return -10000;
		}
		for (int i = 0; i < rows; i++){
			for (int j = 0; j <cols; j++){
				if (board[i][j] == 'b')
					sum -= evaluationTable[i][j];
				else if (board[i][j] == 'r')
					sum += evaluationTable[i][j];
			}
		}
		//System.out.println(sum);
		return sum;
	}//end method evalutationFunction
	
	//turn methods
	public void changeTurn(){
		turn = (turn+1)%2;
	}//end method changeTurn
	
	public void setTurn(int t){
		turn = t;
	}//end method setTurn
	
	public int getTurn(){
		return turn;
	}//end method getTurn
	
	public Point play(Move tryIt){
		Point result;
		
		//determine row
		
		//go for lowest row
		Point temp = new Point(0, tryIt.getMove());
		//System.out.println(temp.row + " " + temp.col);
		if(board[temp.row][temp.col]!='e'){
			result = new Point(-99,-99);//col is full
			return result;
		}
		
		while(temp.row+1<rows && board[temp.row+1][temp.col]=='e'){//while the next row in that col is empty, set to next row down
			temp.setRow(temp.row+1);
		}
		
		//mark board with correct color
		if(turn==0){
			board[temp.row][temp.col] = 'b';
		}//user turn
		else if(turn==1){
			board[temp.row][temp.col] = 'r';
		}//user turn
		
		//return point with (row, col)
		result = new Point(temp.row, temp.col);
		return result;
	}//end method play
	
	public void legalMoves(Stack<Move> moves){
		//push all legal moves onto the stack
		for(int i=0; i<cols; i++){
			Move point = new Move(i);
			if(board[0][i] != 'b' && board[0][i] != 'r'){
				moves.push(point);
			}
		}//go through each column
	}//end method legalMoves
	
	public int worstCase(){
		if(turn==1)//max
			return -10000;
		return 10000;
	}//end method worstCase
	
	public boolean better(int value, int oldValue){
		if(turn==1)
			return (value>=oldValue);
		return (value<=oldValue);
	}//end method better
	
	public int lookAhead(Board game, int depth, Move recommended){
		game.changeTurn();
		if(game.done() || depth==0){
			return game.evaluationFunction();
		}//end if
		else{
			Stack<Move> moves = new Stack<Move>();
			game.legalMoves(moves);
			int value;
			int bestValue = game.worstCase();
			while(!moves.empty()){
				Move move = new Move();
				Move tryIt = moves.peek();
				Board newGame = new Board(game);
				newGame.play(tryIt);
				
				value = lookAhead(newGame, depth-1, move);
				if(game.better(value, bestValue)){
					//System.out.println("value: " + value + " best value: " + bestValue);
					bestValue = value;
					recommended.setMove(tryIt);
				}
				moves.pop();
			}
			return bestValue;
		}//end else
		
	}//end method lookAhead
	
	public void setBoard(char[][] board){
		this.board = board;
	}//end method setBoard
	
	public void printBoard(){
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				System.out.print(board[i][j]);
			}//end inner for loop
			
			System.out.println();
		}//end outer for loop
	}//end method printBoard
	
	
}//end class Board
