package Question1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ConnectFourGUI extends JFrame{
	//panels
	private JPanel boardPanel;
	private JPanel buttonPanel;
	
	//buttons
	private JButton[] columnButtons;
	private JButton newGameButton;
	private JButton exitButton;
	
	//labels
	private JLabel[][] board;
	
	//other
	private boolean done;
	private Board game;
	private int pickCol;
	
	public ConnectFourGUI(){
		super("Connect Four");
		done = false;
		game = new Board();
		
		//board panel
		boardPanel = new JPanel();
		
		board = new JLabel[6][7];//6 rows, 7 cols
		for(int i=0; i<game.rows; i++){
			for(int j=0; j<game.cols; j++){	
				if(game.board[i][j]=='e')board[i][j] = new JLabel(new ImageIcon(this.getClass().getResource("blankCircle.jpg")));
				else if(game.board[i][j]=='r')board[i][j] = new JLabel(new ImageIcon(this.getClass().getResource("redCircle.gif")));
				if(game.board[i][j]=='b')board[i][j] = new JLabel(new ImageIcon(this.getClass().getResource("blackCircle.gif")));
				boardPanel.add(board[i][j]);	
			}
		}
		
		columnButtons = new JButton[7];
		for(int k=0; k<7; k++){
			columnButtons[k] = new JButton("" + k);
			columnButtons[k].addActionListener(new ButtonListener());
			boardPanel.add(columnButtons[k]);
		}
	
		boardPanel.setLayout(new GridLayout(game.rows+1, game.cols));
		add(boardPanel, BorderLayout.CENTER);
		
		//button panel
		buttonPanel = new JPanel();
		
		newGameButton = new JButton("New Game");
		exitButton = new JButton("Exit");
		newGameButton.addActionListener(new ButtonListener());
		exitButton.addActionListener(new ButtonListener());
		
		buttonPanel.add(newGameButton);
		buttonPanel.add(exitButton);
		
		add(buttonPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		setResizable(false);
        setBounds(0,0,600,700);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}//end default constructor
	
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){	
			if(e.getSource()==exitButton){
				System.exit(0);
			}//exit button
			else if(e.getSource()==newGameButton){
				ConnectFourGUI c1 = new ConnectFourGUI();
			}//new game
			else{
				for(int i=0; i<7; i++){
					if(e.getSource()==columnButtons[i]){
						makeMove(i);
					}//column button
				}//check each column button in array
			}//column buttons
		}//end method actionPerformed
	}//end class ActionListener
	
	void makeMove(int userCol){
		if(!done){
			Move userMove = new Move(userCol);
			game.play(userMove);
			redrawBoard();
			done = game.done();
			if(done){
				endGame();
				return;
			}
			
			game.changeTurn();
			
			Move recommended = new Move();
			Board newGame = new Board(game);
			game.lookAhead(newGame, 6, recommended);
			int t = recommended.getMove();
			//System.out.println(t);
			Move temp = new Move(t);
			game.play(temp);
			redrawBoard();
			done = game.done();
			if(done){
				endGame();
				return;
			}
			game.changeTurn();
		}//game not done
		
	}//end method makeMove
	
	void redrawBoard(){
		for(int i=0; i<game.rows; i++){
			for(int j=0; j<game.cols; j++){	
				if(game.board[i][j]=='r')board[i][j].setIcon(new ImageIcon(this.getClass().getResource("redCircle.gif")));
				else if(game.board[i][j]=='b')board[i][j].setIcon(new ImageIcon(this.getClass().getResource("blackCircle.gif")));
			}
		}
		checkFullCol();
		repaint();
	}//end method redrawBoard
	
	void checkFullCol(){
		for(int j=0; j<game.cols; j++){	
			boolean full = true;
			for(int i=0; i<game.rows; i++){
				if(game.board[i][j] == 'e'){
					full = false;
				}
			}
			if(full){
				columnButtons[j].setEnabled(false);
			}
			
		}
	}//end method checkFullCol
	
	void endGame(){
		if(game.winner()=='b'){
			int answer = JOptionPane.showConfirmDialog(null, "So shines a good deed in a weary world. Charlie? My boy. You've won! You did it! You did it! I knew you would! \nI had to test you, Charlie. And you passed the test! You won! The jackpot, my dear sir! The grand and glorious jackpot! \nThe chocolate, yes! The chocolate, but that's just the beginning! We hafta get on! We hafta get on! We have so much time, and so little to do! Strike that. Reverse it. This way, please! \n Play Again?", "Circles Game", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(answer == JOptionPane.NO_OPTION){
				System.exit(0);
			}
			else if(answer == JOptionPane.YES_OPTION){
				ConnectFourGUI c1 = new ConnectFourGUI();
			}
		}
		else if(game.winner()=='r'){
			int answer = JOptionPane.showConfirmDialog(null, "'You get nothing! You loose! Good day sir!' \n Play again?", "Circles Game", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(answer == JOptionPane.NO_OPTION){
				System.exit(0);
			}
			else if(answer == JOptionPane.YES_OPTION){
				ConnectFourGUI c1 = new ConnectFourGUI();
			}	
		}
	}//end method endGame
	
	public static void main(String [] args){
		ConnectFourGUI c = new ConnectFourGUI();
	}//end main method
	
}//end class ConnectFourGUI
