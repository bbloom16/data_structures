package Question1;
import java.util.*;

public class ConnectFour {
	boolean done;
	Scanner kb;
	Board game;
	
	public ConnectFour(){
		kb = new Scanner(System.in);
		done = false;
		game = new Board();
		playGame();
	}//end default constructor
	
	public void playGame(){

		int userCol;
		Move userMove;
		Move compMove;
		
		game.printBoard();
		
		do{
			
			System.out.print("Choose a column: ");
			int tempUserCol = kb.nextInt();
			
			if(tempUserCol>-1 && tempUserCol<7){
				userCol = tempUserCol;
				
				System.out.println("User move: ");
				userMove = new Move(userCol);
				game.play(userMove);
				game.printBoard();
				done = game.done();
				if(done){
					if(game.winner()=='b')System.out.println("Game over you win!");
					else if(game.winner()=='r')System.out.println("Game over computer wins!");
					return;
				}
				game.changeTurn();
				
				System.out.println("Computer move: ");
				Move recommended = new Move();
				Board newGame = new Board(game);
				game.lookAhead(newGame, 5, recommended);
				int t = recommended.getMove();
				//System.out.println(t);
				Move temp = new Move(t);
				game.play(temp);
				game.printBoard();
				done = game.done();
				if(done){
					if(game.winner()=='b')System.out.println("Game over you win!");
					else if(game.winner()=='r')System.out.println("Game over computer wins!");
					return;
				}
				game.changeTurn();
			}
			
		}while(!done);
	}//end method playGame
	
	public static void main(String [] args){
		ConnectFour c = new ConnectFour();
	}//end main method
	
}//end class ConnectFour
