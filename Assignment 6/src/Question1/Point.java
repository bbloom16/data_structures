package Question1;

public class Point {
	int row;
	int col;
	
	public Point(){
		row = 0;
		col = 0;
	}
	
	public Point(int row, int col){
		this.row = row;
		this.col = col;
	}
	
	public void setRow(int row){
		this.row = row;
	}
	
	public void setCol(int col){
		this.col = col;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getCol(){
		return col;
	}
	
}
