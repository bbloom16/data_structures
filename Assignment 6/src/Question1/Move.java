package Question1;

public class Move {
	private int column;

	public Move(){
		 column = 0;
	}//end default constructor

	public Move(int n){
		column = n;
	}//end one arg constructor

	public void setMove(Move n){
		column = n.getMove();
	}//end method setMove

	public int getMove(){
		return column;
	}//end method getMove
	
	public String toString(){
		return column + "";
	}//end method toString
	
}//end class Move
