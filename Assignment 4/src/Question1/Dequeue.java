package Question1;

public class Dequeue<E> {
	
	private class Node{
		Node prev;
		E data;
		Node next;
		
		public Node(){
			prev = null;
			data = null;
			next = null;
		}//end default constructor
		
		public Node(E data){
			prev = null;
			this.data = data;
			next = null;
		}//end one-arg constructor
	}//end private class Node
	
	Node front, rear;
	int size;
	
	public Dequeue(){
		front = rear = null;
		size = 0;
	}//end default constructor
	
	void insertFront(E x){
		Node p = new Node(x);
		
		if(size==0){
			front = rear = p;
		}
		else{
			p.next = front;
			p.prev = null;
			front = p;
		}
		size++;
	}//end method insertFront
	
	void insertRear(E x){
		Node p = new Node(x);
		if(size==0){
			front = rear = p;
		}
		else{
			p.prev = rear;
			p.next = null;
			rear = p;
		}
		size++;
	}//end method insertRear
	
	E removeFront(){
		if (size == 0){
            System.out.println("Queue Underflow");
            System.exit(0);
        }
		Node tempNode = front;//stores existing front
		front = front.next;
		
		size--;
		
		E temp = tempNode.data;
		
		return temp;
	}//end method removeFront
	
	E removeRear(){
		if (size == 0){
            System.out.println("Queue Underflow");
            System.exit(0);
        }
		Node tempNode = rear;
		rear = rear.prev;
		
		size--;
		
		E temp = tempNode.data;
		
		return temp;
	}//end method removeRear
	
	boolean empty(){
		return size == 0;
	}//end method empty
	
	int size(){
		return size;
	}//end method size
	
	E peekFront(){
		if (size == 0){
            System.out.println("Queue Underflow");
            System.exit(0);
		}
		E temp = front.data;
	     
		return temp;
	}//end method peekFront
	
	E peekRear(){
		if (size == 0){
            System.out.println("Queue Underflow");
            System.exit(0);
        }
		E temp = rear.data;
		return temp;
	}//end method peekRear
	
	public static void main(String [] args){
		Dequeue<String> d = new Dequeue<String>();
		d.insertFront("Dopey");
		d.insertFront("Happy");
		d.insertRear("Doc");
		System.out.println(d.removeFront());
		System.out.println("Deque size is "+ d.size());
		System.out.println(d.removeRear());
		System.out.println(d.removeFront());
		System.out.println("Deque size is "+ d.size());
		d.insertRear("Sleepy");
		System.out.println(d.removeFront());
		System.out.println("Deque size is "+ d.size());
	}//end main
	
}//end class Dequeue
