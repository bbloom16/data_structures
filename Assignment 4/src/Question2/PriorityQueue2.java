package Question2;

import java.util.*;
public class PriorityQueue2<E>
{
  // inserts data into the queue in order of priority
 // so the element with highest priority (lowest number) is first
 // remove() takes the  first element from queue

 
    private class Node
    {
        E data;
        int priority;
        Node next;
        public Node()  //default constr
        {
            data = null;
            priority = 0;
            next = null;
        }
        public Node (E data,int priority) // one arg con
        {
            this.data = data;
            this.priority = priority;
            next = null;
            }
        }

        Node front;
        int size;

        public PriorityQueue2()
        {
            front = null;
            size = 0;
        }
        public void insert(PriorityData<E> x)
        {
            //inserts at beginning of queue
            Node p = new Node(x.getData(), x.getPriority());
            
            if(size==0){
            	front = p;
            }
            else{
            	Node temp = front;
            	front = p;
            	front.next = temp;
            }
            size++;
        }
        public PriorityData<E> remove()
        {
           if (size == 0)
           {
                System.out.println("Priority Queue underflow");
                System.exit(0);
           }
          
           Node q = front;
           Node r = null;
           int highestP = front.priority;
           Node highestN = front;
           Node beforeN = null;
           while (q != null) // stops when exhausts the list
           {
        	   int priority = q.priority;
        	   if(priority<=highestP){
            	   highestP = priority;
            	   highestN = q;
            	   beforeN = r;
               }//finds the lowest priority node
               r = q;
        	   q = q.next;
           }
           
           if(beforeN==null){
        	   front = highestN.next;
           }
           else{
        	   beforeN.next = highestN.next;
           }          
          
          //save highest node
           PriorityData<E> temp = new PriorityData<E>();
           temp.setData(highestN.data);
           temp.setPriority(highestN.priority);

           size--;
           return temp;

        }
}
