package Question2;

import java.util.*;
public class TestPQ2
{
    public static void main(String args[])
    {
        Scanner input = new Scanner (System.in);
        PriorityData<String> pd;
        PriorityQueue2<String> q = new PriorityQueue2<String>();
        for (int i = 0; i < 5; i++)
        {
            System.out.println("Enter a name and a priority: ");
            String n = input.next();

            int p = input.nextInt();
            pd= new PriorityData<String>(n,p);  // make a PriorityData object
            q.insert(pd); // insert the data and priority into the queue
        }

        System.out.println("The data in order of priority are:");
        for (int i = 0; i < 5; i++)
        {
            pd = q.remove();
            System.out.println(pd.getData() + " "+ pd.getPriority()); // print data with priority

        } 
    }//end main
}//end class TestPQ2
