package Question3;
import java.util.*;
import java.io.*;

public class FourColor {
	String[] colors = {"red", "green", "yellow", "blue"};
	String[] states = new String[49];
	boolean[] visited = new boolean[49];
	char[][] matrix = new char[49][49];
	Stack<Integer> moveStack = new Stack<Integer>();
	Stack<Integer> stateStack = new Stack<Integer>();
	int color = 0;
	
	public FourColor(){
		try{
			Scanner file = new Scanner(new File("matrix.txt"));//open file
			
			for(int i=0; i<states.length; i++){
				String text = file.nextLine();
				states[i] = text.substring(text.indexOf(" ")+1);
				visited[i] = false;
			}
			
			file.nextLine();//space before matrix
			
			for(int j=0; j<49; j++){
				String text = file.nextLine();
				for(int k=0; k<49; k++){
					matrix[k][j] = text.charAt(k);
				}
			}
			
			file.close();
		}//end try
		catch(FileNotFoundException e){
			System.out.println("File not found.");
			System.exit(0);
		}//end catch
	}//end default constructor
	
	public void nextStop(int num){
		int a = stateStack.peek();
		if(a>3){
			color = 0;
			a = color;
		}
		System.out.println(states[num] + " " + colors[a]);
		visited[num] = true;
		moveStack.push(num);
	}//end method nextStop
	
	public boolean gameOver(){
		boolean end = true;
		for(int i=0; i<visited.length; i++){
			if(visited[i]==false)
				end = false;
		}
		return end;
	}//end method gameOver
	
	public int notVisited(int num){
		for(int i=0; i<49; i++){
			if(matrix[i][num]=='1'){
				if(visited[i]== false){
					return i;
				}//end if
			}//end if
		}//end for
		
		return -1;
	}//end method notVisited
	
	public void fillColors(){
		int last = 0;
		stateStack.push(color);
		nextStop(47);
		while(!moveStack.empty()){
			int state = notVisited(moveStack.peek());
			
			if(state != -1){
				color++;
				stateStack.push(color);
				nextStop(state);
				last = state;
			}
			else{
				stateStack.pop();
				int lastStack = moveStack.pop();
				if(last != lastStack){
					//System.out.println("Went back to " + states[lastStack]);
				}
			}
			
			
		}//end while
		System.out.println("\nTrip finished");
	}//end method fill
	
	public static void main(String [] args){
		FourColor f = new FourColor();
		f.fillColors();
	}//end main method
}//end class FourColor
