package Question1;
import java.util.*;
import java.io.*;

public class Mouse {
	int rows;
	int cols;
	int[][] maze;
	boolean[][] visited;
	boolean gameOver = false;
	Stack <Coordinates> moves= new Stack <Coordinates>(100);
	
	//start pos
	int startRow;
	int startCol;
	//end pos
	int endRow;
	int endCol;
	
	public Mouse(){		
		try{
			Scanner file = new Scanner(new File("maze.txt"));//open file
			rows = file.nextInt();
			cols = file.nextInt();
			
			maze = new int[rows][cols];
			visited= new boolean[rows][cols];
			
			//fill array from file
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					if(file.hasNextInt()){
						int pos = file.nextInt();
						if(pos==2){
							startRow = i;
							startCol = j;
						}//starting pos
						else if(pos==3){
							endRow = i;
							endCol = j;
						}//finish pos
						
						maze[i][j] = pos;
						//System.out.println(maze[i][j]);
					}
				}//end inner for
			}//end outer for
				
			file.close();
		}//end try
		catch(FileNotFoundException e){
			System.out.println("File Not Found");
			System.exit(0);
		}//end catch
		
		//set all positions as not visited
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				visited[i][j] = false;
			}
		}
		
	}//end default constructor
	
	public void printMaze(){
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				System.out.print(maze[i][j]);
			}
			System.out.println();
		}
	}//end method printMaze
	
	public void printGoals(){
		System.out.println("Starting position: " + startRow + ", " + startCol);
		System.out.println("Goal: " + endRow + ", " + endCol);
	}//end method printGoals
	
	public void visited(int row, int col){
		//System.out.println(row + " " + col);
		visited[row][col] = true;
	}//end method visited
	
	public void move(){
		visited(startRow,startCol);
		Coordinates c1 = new Coordinates(startRow, startCol);
		moves.push(c1);
		int r,c;
		
		while(!gameOver){
				Coordinates a = moves.peek();
				r = a.x;
				c = a.y;
				
				//System.out.println(r + " " + c);
				
				if(checkSpot(r, c+1)){
					//System.out.println("right");
					c++;
					Coordinates b = new Coordinates(r,c);
					moves.push(b);
				}//look right
				else if(checkSpot(r+1, c)){
					//System.out.println("down");
					r++;
					Coordinates b = new Coordinates(r,c);
					moves.push(b);
				}//look down
				else if(checkSpot(r, c-1)){
					//System.out.println("left");
					c--;
					Coordinates b = new Coordinates(r,c);
					moves.push(b);
				}//look left
				else if(checkSpot(r-1, c)){
					//System.out.println("up");
					r--;
					Coordinates b = new Coordinates(r,c);
					moves.push(b);
				}//look up
				else{
					//System.out.println("pop");
					moves.pop();
				}//go back, pop the stack
				
				visited(r,c);
				
				if(moves.empty()){
					System.out.println("Trapped  - no way out!");
					System.exit(0);
				}
				
				if(visited[endRow][endCol]==true){
					gameOver = true;

					moves.pop();
					while(moves.size()>1){
						Coordinates d = moves.pop();
						maze[d.x][d.y] = 8;
					}
				}//game over, got to the end of the maze
		}//end while
		
		
	}//end method move
	
	public boolean checkSpot(int row, int col){
		boolean go = false;
		if(((maze[row][col]==0)||maze[row][col]==3)&&(visited[row][col]==false)){
			go = true;
		}//end if
		
		return go;
	}//end method checkSpot
	
	public static void main(String [] args){
		Mouse m = new Mouse();
		System.out.println("--------------Maze--------------");
		m.printMaze();
		System.out.println();
		m.printGoals();
		m.move();
		System.out.println("--------------Maze Solution--------------");
		System.out.println("8s are path to follow");
		m.printMaze();
	}//end main method
	
}//end class Mouse
