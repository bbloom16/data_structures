package Question2;
import java.util.*;
import java.io.*;

public class Jack {
	String[] states = new String[49];
	boolean[] visited = new boolean[49];
	char[][] matrix = new char[49][49];
	Stack<Integer> moveStack = new Stack<Integer>();
	
	public Jack(){
		try{
			Scanner file = new Scanner(new File("matrix.txt"));//open file
			
			for(int i=0; i<states.length; i++){
				String text = file.nextLine();
				states[i] = text.substring(text.indexOf(" ")+1);
				visited[i] = false;
			}
			
			file.nextLine();
			
			for(int j=0; j<49; j++){
				String text = file.nextLine();
				for(int k=0; k<49; k++){
					matrix[k][j] = text.charAt(k);
				}
			}
		}//end try
		catch(FileNotFoundException e){
			System.out.println("File not found.");
			System.exit(0);
		}//end catch
	}//end default constructor
	
	public void nextStop(int num){
		System.out.println("Visited " + states[num]);
		visited[num] = true;
		moveStack.push(num);
	}//end method nextStop
	
	public boolean gameOver(){
		boolean end = true;
		for(int i=0; i<visited.length; i++){
			if(visited[i]==false)
				end = false;
		}
		return end;
	}//end method gameOver
	
	public int notVisited(int num){
		for(int i=0; i<49; i++){
			if(matrix[i][num]=='1'){
				if(visited[i]== false){
					return i;
				}//end if
			}//end if
		}//end for
		
		return -1;
	}//end method notVisited
	
	public void trip(){
		int last = 0;
		nextStop(47);
		while(!moveStack.empty()){
			int state = notVisited(moveStack.peek());
			
			if(state != -1){
				nextStop(state);
				last = state;
			}
			else{
				int lastStack = moveStack.pop();
				if(last != lastStack){
					System.out.println("Went back to " + states[lastStack]);
				}
			}
			
			
		}//end while
		System.out.println("\nTrip finished");
	}//end method trip
	
	public static void main(String [] args){
		Jack j = new Jack();
		j.trip();
	}//end main method
	
}//end class Jack