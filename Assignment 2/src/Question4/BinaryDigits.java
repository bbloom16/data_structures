package Question4;
import java.util.*;
import java.io.*;

public class BinaryDigits {
	public static void bitChecker(String input){
		Stack<Character> stacky = new Stack<Character>();
		int a = 0;
		for(int i=0; i<input.length(); i++){
			char c = input.charAt(i);
			
			if(c=='1'){
				stacky.push(c);
			}
			else if(c=='0'){
				if(stacky.empty()){
					a++;
				}
				else{
					stacky.pop();
				}
			}
		}//end for loop
		
		if(stacky.size()==a){
			System.out.println("Same number of 0's and 1's");
		}
		else if(stacky.size()>a){
			System.out.println("Different number of 0's and 1's. " + (stacky.size()-a) + " additional 1's");
		}
		else if(stacky.size()<a){
			System.out.println("Different number of 0's and 1's. " + (a-stacky.size()) + " additional 0's");
		}
		
	}//end method bitChecker
	
	public static void main(String [] args){
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Enter a string of bits. Enter 11111 to exit: ");
		String input = kb.next();
		
		while(!input.equals("11111")){
			bitChecker(input);
			System.out.print("Enter a string of bits. Enter 11111 to exit: ");
            input = kb.next();
		}//end while
	}//end main method
	
}//end class BinaryDigits
