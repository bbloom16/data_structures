package Question2;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;

public class Ruler extends JPanel{
	
	public void paintComponent(Graphics g){
		super.paintComponents(g);
		
		g.setColor(Color.BLUE);
		g.drawLine(0, 300, 800, 300);//set ruler line
		
		halfLine(0,400, 100, g);
		halfLine(400,800,100, g);
	}//end method paintComponents
	
	public int halfLine(int left, int right, int height, Graphics g){
		//System.out.println(left+ " " + right);
		int top = 300-height;
		g.drawLine(left, top, left, 300);
		if(right-left<60){
			return 0;
		}
		int y = (right+left)/2;
		return halfLine(left,y,(height/2),g) + halfLine(y+1,right,(height/2),g) + 1;
	}//end method halfLine
	
	public static void main(String [] args){
		JFrame f = new JFrame("Ruler");
		Ruler r = new Ruler();
		f.add(r);
		f.setBounds(0,0,800,350);
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}//end main method
	
}//end class Ruler
