package Question2;

public class Coordinates {
	int a;
	int b;
	int c;
	int d;
	
	public Coordinates(int a, int b, int c, int d){
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	public int getA(){
		return a;
	}
	
	public int getB(){
		return b;
	}
	
	public int getC(){
		return c;
	}
	
	public int getD(){
		return d;
	}
	
	public void setA(int var){
		a = var;
	}
	
	public void setB(int var){
		b = var;
	}
	
	public void setC(int var){
		c = var;
	}
	
	public void setD(int var){
		d = var;
	}
}
