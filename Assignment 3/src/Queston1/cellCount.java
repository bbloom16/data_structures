package Queston1;

import java.io.*;
import java.util.*;

public class cellCount {
	int rows;
	int cols;
	int[][] grid;	
	
	public cellCount(){
		try{
			Scanner file = new Scanner(new File("grid.txt"));//open file
			
			rows = file.nextInt();
			cols = file.nextInt();
			
			grid = new int[rows][cols];
			while(file.hasNextLine()){
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						grid[i][j] = file.nextInt();
					}
				}
			}
			
			file.close();
		}//end try
		catch(FileNotFoundException e){
			System.out.println("File not found.");
			System.exit(0);
		}//end catch
		
		System.out.println("Maze: ");
		printMaze();//prints the initial board
		System.out.println("----------------------------------------");
		
		int areas = 0;
		for(int i=1; i<rows; i++){
			for(int j=1; j<cols; j++){
				int count = findWhite(i,j);
				
				if(count>0){
					areas++;
					System.out.println("Area " + areas + " has " + count + " white spaces.");
				}
			}
		}
	}//end default constructor
	
	public int findWhite(int row, int col){
		if(grid[row][col]==0){
			grid[row][col] = 2;//mark visited
			return 1 + findWhite(row+1, col) + findWhite(row, col+1) + findWhite(row-1, col) + findWhite(row, col-1);
		}
		else{
			return 0;
		}
	}//end method findWhite	
	
	public void printMaze(){
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				System.out.print(grid[i][j]);
			}
			System.out.println();
		}
	}//end method printMaze
	
	public static void main(String [] args){
		cellCount c = new cellCount();
	}//end main method
	
}//end class cellCount
