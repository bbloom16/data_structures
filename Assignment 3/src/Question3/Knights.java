package Question3;

import java.util.*;

public class Knights {
	boolean finished = false;
	int board[][] = new int[8][8];
	int[] posMoveRow = {1,2,2,1,-1,-2,-2,-1};//xPos for possible moves, paired with corresponding yPos
	int[] posMoveCol = {-2,-1,1,2,2,1,-1,-2};//yPos for possible moves, paired with corresponding xPos
	int knightCount = 1;
	
	private class Position {
		int row;
	    int col;
	    int count;
	    
	    public Position(int r, int c, int ct){
	      this.row = r;
	      this.col = c;
	      this.count = ct;
	    }//end constructor
	    
	}//end class Move
	
	public Knights(int row, int col){
		for(int i=0; i<8; i++){
			for(int j=0; j<8; j++){
				board[i][j]=0;
			}
		}
		
		board[row][col] = knightCount;
		placeKnight(row, col);
	}//end constructor
	
	public void placeKnight(int row, int col, int count){
		
	}//end method placeKnight
	
	public Position[] findMoves(int row, int col){
		
		Position[] potentialMoves = new Position[8];//8 possible moves from every location, not all viable
		Position[] moves;
		int count = 0;//count valid moves
		for(int i=0; i<8; i++){
			int newRow = row+posMoveRow[i];
			int newCol = col+posMoveCol[i];
			if((newRow>7) || (newRow<0) || (newCol>7) || (newCol<0) || (board[newRow][newCol]!=0)){
				potentialMoves[i] = new Position(-1,-1,i);
			}//cannot use position
			else{
				count++;//increase valid moves
				potentialMoves[i] = new Position(newRow,newCol,i);
			}//valid move
		}//go through all possible moves

		if(count==0){
			finished = true;
			//printBoard();
			
			moves = new Position[0];
		}//no more moves to make
		
		else{
			moves = new Position[count];//array of valid positions, determined by count
			int z=0;
			for(int j=0; j<potentialMoves.length; j++){
				if((potentialMoves[j].row != -1) && (potentialMoves[j].col != -1)){
					moves[z] = potentialMoves[j];
					z++;
				}//if move is valid, add as move
			}//go through all potential moves
		}
		
		
		return moves;
	}//end method findMoves
	
	public Position nextMove(int row, int col, Position[] moves){
		Position result;
		int leastExits = 100;
		int bestMove = -1;
		
		if(knightCount==63){
			result = moves[0];//last move
		}
		else{
			for(int i=0; i<moves.length; i++){
				
				int numExits = findMoves(moves[i].row, moves[i].col).length;//number of possible moves from each position
				
				if((numExits>0)&&(numExits<leastExits)){
					
					leastExits = numExits;
					bestMove = i;
				}
			}
			//System.out.println(bestMove);
			result = moves[bestMove];
		}
		
		return result;
	}//end method nextMove 
	
	public void placeKnight(int row, int col){
		
		Position[] moves = findMoves(row, col);
		if(moves.length==0)
			return;
		
		Position move = nextMove(row, col, moves);

		knightCount++;
		
		board[move.row][move.col] = knightCount;
		
		if(finished==false){
			placeKnight(move.row, move.col);
		}
	}//end method placeKnight
	
	public void printBoard(){
		for(int i=0; i<8; i++){
			for(int j=0; j<8; j++){
				System.out.print(board[i][j] + "\t");
			}
			System.out.println();
		}
	}//end method printBoard
	
	/*public static void main(String [] args){
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Enter starting row and column: ");
		int row = kb.nextInt();
		int col = kb.nextInt();
		
		Knights k = new Knights(row, col);
		
		kb.close();
	}*/
}
