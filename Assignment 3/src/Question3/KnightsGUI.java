package Question3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class KnightsGUI extends JFrame{
	private JButton[][] buttons;
	private int rows = 8;
	private int cols = 8;
	private JPanel buttonPanel;
	private JPanel exitPanel;
	private JButton exitButton;
	
	public KnightsGUI(){
		super("Knight Tour");
		
		buttonPanel = new JPanel();
		
		buttons = new JButton[rows][cols];
		
		int num = 1;
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				buttons[i][j] = new JButton();
				buttons[i][j].setText(num+"");
				buttons[i][j].addActionListener(new ButtonListener());
				if(((j%2==0) && (i%2==0))||((j%2==1) && (i%2==1))){
					buttons[i][j].setBackground(Color.RED);;
				}
				else{
					buttons[i][j].setBackground(Color.WHITE);;
				}
				buttonPanel.add(buttons[i][j]);
				num++;
			}
		}
		buttonPanel.setLayout(new GridLayout(rows, cols));
		add(buttonPanel, BorderLayout.CENTER);
		
		exitPanel = new JPanel();
		exitButton = new JButton("Exit");
		exitButton.addActionListener(new ButtonListener());
		exitPanel.add(exitButton);
		
		add(exitPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		setResizable(false);
        setBounds(0,0,600,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}//end default constructor
	
	public void drawBoard(int[][] board){
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				buttons[i][j].setText(board[i][j]+"");
			}
		}
		
		repaint();
	}//end method drawBoard
	
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){	
			if(e.getSource()==exitButton){
				System.exit(0);
			}//postFixButton
			else {
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						if(e.getSource()==buttons[i][j]){
							int row = i;
							int col = j;
							
							Knights k = new Knights(row, col);
							int[][] board = k.board;
							
							drawBoard(board);
						}
					}//end inner for
				}//end outer for
			}//end else
		}//end method actionPerformed
		
	}//end class ButtonListener
	
	public static void main(String [] args){
		KnightsGUI k = new KnightsGUI();
	}//end main method
}//end class KnightsGUI
